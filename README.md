# orgmode-char-templates

Repository for tabletop character templates written in org-mode. Feel free to
submit your own. The repo will also contain a list of links to other templates.

## Other templates
- [Feature-rich D&D 5e template](https://github.com/jrbp/org-mode-dnd-5e-character-template)
