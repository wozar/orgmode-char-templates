* Character
  - Name:
  - Race:
  - Class:
  - Level:
  - XP:
  - Alignment:
  - Background:
  - Inspiration:
  - Proficiency bonus:
  - Passive Wisdom (Perception):

* Ability scores
|--------------+-------+-----|
| Ability      | Score | Mod |
|--------------+-------+-----|
| Strength     |       |     |
| Dexterity    |       |     |
| Constitution |       |     |
| Intelligence |       |     |
| Wisdom       |       |     |
| Charisma     |       |     |
|--------------+-------+-----|

* Skills
|-----------------+-----+------+---------|
| Skill           | Mod | Prof | Ability |
|-----------------+-----+------+---------|
| Acrobatics      |     |      | Dex     |
| Animal Handling |     |      | Wis     |
| Arcana          |     |      | Int     |
| Athletics       |     |      | Str     |
| Deception       |     |      | Cha     |
| History         |     |      | Int     |
| Insight         |     |      | Wis     |
| Intimidation    |     |      | Cha     |
| Investigation   |     |      | Int     |
| Medicine        |     |      | Wis     |
| Nature          |     |      | Int     |
| Perception      |     |      | Wis     |
| Performance     |     |      | Cha     |
| Persuasion      |     |      | Cha     |
| Religion        |     |      | Int     |
| Sleight of Hand |     |      | Dex     |
| Stealth         |     |      | Dex     |
| Survival        |     |      | Wis     |
|-----------------+-----+------+---------|

* Saving throws
|--------------+-----+------|
| Ability      | Mod | Prof |
|--------------+-----+------|
| Strength     |     |      |
| Dexterity    |     |      |
| Constitution |     |      |
| Intelligence |     |      |
| Wisdom       |     |      |
| Charisma     |     |      |
|--------------+-----+------|

* Features & Traits
|------+-------------|
| Name | Description |
|------+-------------|
|      |             |
|------+-------------|

* Attacks & Spellcasting
|------+-----------+--------+------|
| Name | Atk Bonus | Damage | Type |
|------+-----------+--------+------|
|      |           |        |      |
|------+-----------+--------+------|

* Inventory
|-------+----|
| Armor | AC |
|-------+----|
|       |    |
|-------+----|

|----------+--------|
| Currency | Amount |
|----------+--------|
| CP       |        |
| SP       |        |
| EP       |        |
| GP       |        |
| PP       |        |
|----------+--------|

|------+--------+-------|
| Name | Weight | Price |
|------+--------+-------|
|      |        |       |
|------+--------+-------|

* Characteristics
** Personal characteristics
|-----------------+---+---|
| Personal traits |   |   |
| Ideals          |   |   |
| Bonds           |   |   |
| Flaws           |   |   |
|-----------------+---+---|

** Physical characteristics
|----------------+-------------|
| Characteristic | Description |
|----------------+-------------|
| Age            |             |
| Height         |             |
| Weight         |             |
| Eyes           |             |
| Skin           |             |
| Hair           |             |
|----------------+-------------|

* Backstory
  Sample text

* Allies & organizations
|------+----------|
| Name | Relation |
|------+----------|
|      |          |
|------+----------|

* Spells
  - Spellcasting class:
  - Spellcasting ability:
  - Spell save DC:
  - Spell attack bonus:

|------+-------------+----------|
|    1 | X           | Y        |
|------+-------------+----------|
| Name | Description | Prepared |
|      |             |          |
|------+-------------+----------|

|------+-------------+----------|
|    2 | X           | Y        |
|------+-------------+----------|
| Name | Description | Prepared |
|      |             |          |
|------+-------------+----------|

|------+-------------+----------|
|    3 | X           | Y        |
|------+-------------+----------|
| Name | Description | Prepared |
|      |             |          |
|------+-------------+----------|

|------+-------------+----------|
|    4 | X           | Y        |
|------+-------------+----------|
| Name | Description | Prepared |
|      |             |          |
|------+-------------+----------|

|------+-------------+----------|
|    5 | X           | Y        |
|------+-------------+----------|
| Name | Description | Prepared |
|      |             |          |
|------+-------------+----------|

|------+-------------+----------|
|    6 | X           | Y        |
|------+-------------+----------|
| Name | Description | Prepared |
|      |             |          |
|------+-------------+----------|

|------+-------------+----------|
|    7 | X           | Y        |
|------+-------------+----------|
| Name | Description | Prepared |
|      |             |          |
|------+-------------+----------|

|------+-------------+----------|
|    8 | X           | Y        |
|------+-------------+----------|
| Name | Description | Prepared |
|      |             |          |
|------+-------------+----------|

|------+-------------+----------|
|    9 | X           | Y        |
|------+-------------+----------|
| Name | Description | Prepared |
|      |             |          |
|------+-------------+----------|
